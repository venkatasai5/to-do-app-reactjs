import React from 'react';
import CreateForm from './CreateForm';
import ListToDo from './ListTo-Do';
import axios from 'axios';
import ReactLoading from 'react-loading'
import { Navbar, Form, Button, Container, Modal } from 'react-bootstrap';
import './App.css';
class App extends React.Component {
  constructor() {
    super()
    const today = new Date();
    const year = today.getFullYear();
    const day = today.getDate();
    const month = today.getMonth() + 1;

    this.state = {
      Subject: '',
      Description: '',
      Status: '',
      CreatedDate: year + "-0" + month + "-" + day,
      ReminderDate: '',
      tasklist: [],
      toggle: false,
      Created: false,
      message: '',
      isLoading: false,
      loadingTypes: ["blank", "balls", "bars", "bubbles", "cubes", "cylon", "spin", "spinningBubbles", "spokes"]
    };
    this.OnloadGetData=this.OnloadGetData.bind(this);
  }

  componentDidMount(){
    this.OnloadGetData();
  }

  OnloadGetData(){
    this.setState({isLoading:true});
    let URL='http://localhost:2020/todolist/tasks/list';
    axios({
      method: 'GET',
      url: URL
    })
    .then(result => {
      let response= result.data.data;
      response= JSON.parse(response);
      console.log("response: ",response);
      this.setState({tasklist:response});
      console.log("TaskList: ",this.state.tasklist);
    })
    .catch(error => {
      console.error('There was an error!', error);
    });
    this.setState({isLoading: false});
  }

  CaptureForm = (event) => {
    this.setState({isLoading: true});
    event.preventDefault();
    const payload = { 
      "subject": this.state.Subject, 
      "description": this.state.Description, 
      "createdDate": this.state.CreatedDate, 
      "reminderDate": this.state.ReminderDate };
    console.log('Payload: ', JSON.stringify(payload))
    let URL='http://localhost:2020/todolist/save/task';
    axios({
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      url: URL,
      data:JSON.stringify(payload)
    })
    .then(result => {
      let response=result.data;
      //response=JSON.parse(response);
      console.log(response);
      if(response.status==='SUCCESS'){
        this.setState({ Created: true });
        this.setState({ message: "Task Saved Successfully..." });
      }else{
        this.setState({ Created: true });
        this.setState({ message: result.data.data });
      }
    })
    .catch(error => {
      console.error('There was an error!', error);
    });
    this.OnloadGetData();
    this.setState({isLoading: false});
  }

  OnChange = (event) => {
    var key = event.target.id;
    var value = event.target.value;
    var dvalue = event.target.defaultValue;
    //console.log({ [key]: value ? value : dvalue });
    this.setState({ [key]: value ? value : dvalue })
    //console.log(this.state.Description);
  }

  Ondelete = (taskId) => {
    this.setState({isLoading: true});
    let URL='http://localhost:2020/todolist/delete/'+taskId;
    axios({
      method: 'DELETE',
      url: URL,
    })
    .then(result => {
      let response=result.data;
      console.log(response);
      if(response.status==='SUCCESS'){
        this.setState({ Created: true });
        this.setState({ message: "Task Deleted Successfully..." });
      }else{
        this.setState({ Created: true });
        this.setState({ message: response.data });
      }
    })
    .catch(error => {
      console.error('There was an error!', error);
    });
    this.OnloadGetData();
    this.setState({isLoading: false});
  }
  
  OnEdit = (task) => {
    this.setState({isLoading: true});
    const { Subject, Description, ReminderDate, CreatedDate, Id } = task;
    const payload = { 
      "subject": Subject, 
      "description": Description, 
      "createdDate": CreatedDate, 
      "reminderDate": ReminderDate ,
      "Id": Id
    };
    let URL='http://localhost:2020/todolist/edit/task';
    axios({
      method: 'PUT',
      url: URL,
      data:JSON.stringify(payload)
    })
    .then(result => {
      let response=result.data;
      if(response.status==='SUCCESS'){
        this.setState({ Created: true });
        this.setState({ message: "Task Updated..." });
      }else if(response.status==='FAILURE'){
        this.setState({ Created: true });
        this.setState({ message: response.data });
      }
    })
    .catch(error => {
      console.error('There was an error!', error);
    });
    this.OnloadGetData();
    this.setState({isLoading: false});
  }

  render() {
    let typeselect=this.state.loadingTypes[Math.floor(Math.random() * this.state.loadingTypes.length)];
    console.log("type: ",typeselect);
    return (
      <div>
        <Navbar className="bg-dark justify-content-between">
          <Form inline>
            <Form.Label className="text-light" onClick={() => { this.setState({ toggle: false })}}><h1>To-Do App</h1></Form.Label>
          </Form>
          <Form inline>
            {this.state.toggle ?
              <Button variant="outline-success" onClick={() => { this.setState({ toggle: !this.state.toggle }) }}>Create To-Do</Button> :
              <Button variant="outline-success" onClick={() => { this.setState({ toggle: !this.state.toggle }) }}>List To-Do</Button>}
          </Form>
        </Navbar>

        {this.state.isLoading ? 
        (<div className="align">
            <ReactLoading type={typeselect} color={"lightgreen"} height={'10%'} width={'10%'}/>
          </div> ) : (
            <div>
        {this.state.toggle ?
          <ListToDo tasklist={this.state.tasklist} Ondelete={this.Ondelete} OnEdit={this.OnEdit}/> :
          <Container>
            <br />
            <CreateForm Subject={this.state.Subject} Description={this.state.Description} 
            ReminderDate={this.state.ReminderDate} OnChange={this.OnChange} CaptureForm={this.CaptureForm} />
          </Container>
        }</div>)}

        < Modal show={this.state.Created} onHide={this.props.Created} >
          <Modal.Body><p>{this.state.message}</p></Modal.Body>
          <Modal.Footer><Button variant="secondary" 
          onClick={() => { this.setState({ Created: false }) }}>Close</Button></Modal.Footer>
        </Modal >
      </div>
    );
  }
}

export default App;