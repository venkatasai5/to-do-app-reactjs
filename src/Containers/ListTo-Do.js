import React from 'react';
import { Table } from 'react-bootstrap';
import Rows from './Rows';
export default class ListApp extends React.Component {
    render() {
        return (<div>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Subject</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>CreatedDate</th>
                        <th>ReminderDate</th>
                        <th>#</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.tasklist.map(
                    (taskItem, index) =><Rows task={taskItem} index={index} {...this.props} />
                    )}
                </tbody>
            </Table>
        </div >);
    }
}