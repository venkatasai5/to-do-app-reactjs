import React from 'react';
import { Button, Form } from 'react-bootstrap';

class Rows extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            Subject: props.task.subject,
            Description: props.task.description,
            Status: props.task.Status,
            CreatedDate: props.task.createdDate,
            ReminderDate: props.task.reminderDate,
            Id:props.task.id
        };
    }

    OnChange = (event) => {
        let key = event.target.id;
        let value = event.target.value;
        this.setState({ [key]: value })
    }


    render() {
        const { index } = this.props;
        return (
            <tr>
                <td>{index + 1}</td>
                <td><Form.Control id="Subject" type="text" value={this.state.Subject} onChange={this.OnChange} /></td>
                <td><Form.Control id="Description" type="textarea" value={this.state.Description} onChange={this.OnChange} /></td>
                <td><Form.Control id="Status" as="select" value={this.state.Status} onChange={this.OnChange}>
                    <option>Active</option>
                    <option>Completed</option>
                </Form.Control></td>
                <td><Form.Control id="CreatedDate" type="text" value={this.state.CreatedDate} /></td>
                <td><Form.Control id="ReminderDate" type="Date" value={this.state.ReminderDate} onChange={this.OnChange} /></td>
                <td>{<Button variant="primary" onClick={this.props.OnEdit.bind(this, {...this.state })}>Update</Button>}</td>
                <td>{<Button variant="warning" onClick={this.props.Ondelete.bind(this, this.state.Id)}>Delete</Button>}</td>
            </tr>
        )
    }

}

export default Rows;