import React from 'react'
import ReactLoading from 'react-loading';
import { Form, Button } from 'react-bootstrap';
export default class CreateForm extends React.Component {

    render() {
        return (
            <div>
        <ReactLoading type="bubbles" color="#fff" />
        < Form onSubmit={this.props.CaptureForm} >
            <Form.Group controlId="Subject">
                <Form.Label>Subject</Form.Label>
                <Form.Control required type="text" placeholder="Enter Subject" value={this.props.Subject} onChange={this.props.OnChange} />
            </Form.Group>

            <Form.Group controlId="Description">
                <Form.Label>Description</Form.Label>
                <Form.Control required as="textarea" placeholder="Enter Description" value={this.props.Description} onChange={this.props.OnChange} />
            </Form.Group>

            <Form.Group controlId="ReminderDate">
                <Form.Label>Reminder Date</Form.Label>
                <Form.Control required type="Date" value={this.props.ReminderDate} onChange={this.props.OnChange} />
            </Form.Group>
            <Button type="submit">Create</Button>
        </Form >
        </div>);
    }
}